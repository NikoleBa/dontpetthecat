﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class bar : MonoBehaviour
{
    public Slider slider;
    private float timer = 0f;

    public void Start()
    {
        Music.instance.GetComponent<Music>().PlayMusic();
    }

    public void setValue(int value)
    {
        slider.value = value;
    }


    public void Update()
    {
        if(Input.GetButton("Jump"))
        {
            timer += Time.deltaTime;
            setValue((int)(timer*1000));
            
            if(slider.value>=2000)
            {
                SceneManager.LoadScene("Game");
            }
        }
        else
        {
            timer = 0;
            setValue(0);
        }
        
    }
}

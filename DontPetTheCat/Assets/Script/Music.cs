﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{

    public static GameObject instance;

    private AudioSource _audioSource;

    private void Awake()
    {
        //GameObject m = GameObject.Find("Music");
        if (instance == null)
        {
            instance = this.gameObject;
            DontDestroyOnLoad(instance);
        }

        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayMusic()
    {
        if (_audioSource.isPlaying) return;
        _audioSource.Play();
    }

}

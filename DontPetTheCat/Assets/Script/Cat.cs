﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cat : MonoBehaviour
{
    public GameObject[] faces;
    public GameObject[] hearts;
    public GameObject bubble;
    public AudioSource hiss;
    public AudioSource chill;
    public AudioSource purr;
    private float timer = 0;
    private bool isPet = false;
    private bool isHappy = false;
    private bool isChill = false;

    // Update is called once per frame
    void Update()
    {//coordinates the states of the faces and shown hearts, depending on the timer
        if (isPet)
        {
            timer = timer + Time.deltaTime;

            if(isHappy)
            {
                return;
            }

            ResetFaces();

            if (timer >= 30)
            {
                faces[3].SetActive(true);
                hearts[2].SetActive(true);
                hearts[1].SetActive(true);
                isHappy = true;
                Win();
                

                if (!purr.isPlaying)
                {
                    purr.Play();
                }
            }
            else if (timer >= 15)
            {
                faces[2].SetActive(true);
                hearts[0].SetActive(true);

                if (!isChill)
                {
                    chill.Play();
                    isChill = true;
                }
            }

            else if (timer > 0)
            {
                faces[1].SetActive(true);
            }

        }
        else
        {

            if(isHappy)
            {
                return;
            }

            ResetFaces();
            ResetHearts();
            timer = 0;
            faces[0].SetActive(true);

        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        isPet = true;
        GameObject hand = GameObject.FindGameObjectWithTag("Hand");
        hand.GetComponent<Animator>().SetTrigger("Enter");

        if(!isHappy)
        {
            hiss.Play();
        }
        


    }

    void OnTriggerExit2D(Collider2D other)
    {
        isPet = false;
        GameObject hand = GameObject.FindGameObjectWithTag("Hand");
        hand.GetComponent<Animator>().SetTrigger("Exit");
        isChill = false;

    }

    void ResetFaces()
    {
      
        for (int i = 0; i < faces.Length; i++)
        {
            faces[i].SetActive(false);
        }
    }

    void ResetHearts()
    {
        if (isHappy)
        {
            return;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            hearts[i].SetActive(false);
        }
    }

    void Win()
    {
        bubble.SetActive(true);
        StartCoroutine(wait());
        
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene("MainMenu");
    }
        
}

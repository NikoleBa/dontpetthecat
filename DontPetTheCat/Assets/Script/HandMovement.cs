﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMovement : MonoBehaviour
{
    private bool isPet = false;
    [SerializeField] private Transform target;
    [SerializeField] private float speed = 4;
    private Vector3 startPosition;
    private bool started = false;

    // Start is called before the first frame update
    IEnumerator wait()
    {
        yield return new WaitForSeconds(1);
        started = true;
    }

    void Start()
    {

        this.startPosition = this.transform.position;
        StartCoroutine(wait());
    }

    // Update is called once per frame
    void Update()
    {
        if(!started)
        {
            return;
        }

        if (Input.GetButton("Jump"))
        {
            MoveToTarget();
        }
        else if (this.transform.position.y < startPosition.y)
        {
            MoveBack();
        }
    }

    private void MoveBack()
    {
        Vector3 direction = target.position - this.transform.position.normalized;
        direction.y = 1;
        this.transform.position = this.transform.position + (direction * speed * Time.deltaTime);
    }

    private void MoveToTarget()
    {
        Vector3 direction = target.position - this.transform.position;
        
        if (Mathf.Abs(direction.y) < 0.5)
        {
            this.transform.position = target.position;
            return;
        }

        direction = direction.normalized;
        direction.y = -1;
        this.transform.position = this.transform.position + (direction * speed * Time.deltaTime);

    }
}
